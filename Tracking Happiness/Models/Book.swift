//
//  Book.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/7/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

enum BookStatus {
    case Reading
    case Pending
}


class Book: Object {
    
    // MARK: - Private properties
    dynamic private var _id: String = ""
    dynamic private var _title: String = ""
    dynamic private var _author: String = ""
    dynamic private var _status: String = kStatusPending
    dynamic private var _numberOfPages: Int = 0
    dynamic private var _bookLogs: List<BookLog> = List<BookLog>()
    dynamic private var _notes: List<BookNote> = List<BookNote>()
    
    // MARK: - Public properties
    dynamic var title: String {
        get { return _title }
        set { _title = newValue }
    }
    
    dynamic var author: String {
        get { return _author }
        set { _author = newValue }
    }
    
    dynamic var status: String {
        get { return _status }
        set { _status = newValue }
    }
    
    dynamic var numberOfPages: Int {
        get { return _numberOfPages }
        set { _numberOfPages = newValue }
    }
    
    dynamic var bookLogs: List<BookLog> {
        get { return _bookLogs }
        set { _bookLogs = newValue }
    }
    
    dynamic var notes: List<BookNote> {
        get { return _notes }
        set { _notes = newValue }
    }
    
    // MARK: - RealmSwift
    override static func primaryKey() -> String {
        return "_id"
    }
    
    override static func ignoredProperties() -> [String] {
        return ["title", "author", "numberOfPages", "status", "bookLogs", "notes"]
    }
    
    // MARK: - Initializers
    required init() {
        super.init()
        _id = NSDate.getTimeStampOfNow(kDateFormat)
    }
    
    override init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    convenience init(title: String, author: String) {
        self.init()
        _title = title
        _author = author
    }
    
    convenience init(title: String, author: String, numberOfPages: Int) {
        self.init(title: title, author: author)
        _numberOfPages = numberOfPages
    }
    
    // MARK: - Private methods
    func getLatestBookLog() -> BookLog? {
        // We also get latest book log based on numberOfReadPages or timestamp.
        // TODO: Need to guarantee that: numberOfReadPages increases along time
        if (_bookLogs.realm == nil) {
            return nil
        }
        
        return _bookLogs.sorted("_numberOfReadPages", ascending: false).first
    }
    
    
    func getCurrentProgressOfReading() -> Float {
        let latestBookLog = getLatestBookLog()
        return latestBookLog?.getProgressOfReading() ?? 0
    }
    
    func getCurrentNumberOfReadPages() -> Int {
        let latestBookLog = getLatestBookLog()
        return latestBookLog?.numberOfReadPages ?? 0
    }
    
    
}
