//
//  Note.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/15/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class BookNote: Object {
    
    // MARK: - Private properties
    dynamic private var _timestamp: String = ""
    dynamic private var _content: String = ""
    
    // MARK: - Public properties
    dynamic var timestamp: String {
        get { return _timestamp }
        set { _timestamp = newValue }
    }
    
    dynamic var content: String {
        get { return _content }
        set { _content = newValue }
    }
    
    // MARK: - RealmSwift
    override static func primaryKey() -> String {
        return "_timestamp"
    }
    
    override static func ignoredProperties() -> [String] {
        return ["timestamp", "content"]
    }
    
    // MARK: - Initializers
    required init() {
        super.init()
        _timestamp = NSDate.getTimeStampOfNow(kDateFormat)
    }
    
    override init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    convenience init(content: String) {
        self.init()
        _content = content
    }
}
