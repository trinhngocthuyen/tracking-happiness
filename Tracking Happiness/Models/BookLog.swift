//
//  BookLog.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/9/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class BookLog: Object {
    
    // MARK: - Private properties
    dynamic private var _timestamp: String = ""
    dynamic private var _numberOfReadPages: Int = 0     // NOTE: Be careful when divide _numberOfReadPages
    
    // MARK: - Public properties
    dynamic var timestamp: String {
        get { return _timestamp }
        set { _timestamp = newValue }
    }
    
    dynamic var numberOfReadPages: Int {
        get { return _numberOfReadPages }
        set { _numberOfReadPages = newValue }
    }
    
    // MARK: - RealmSwift
    override static func primaryKey() -> String {
        return "_timestamp"
    }
    
    override static func ignoredProperties() -> [String] {
        return ["timestamp", "numberOfReadPages"]
    }
    
    // MARK: - Initializers
    required init() {
        super.init()
        _timestamp = NSDate.getTimeStampOfNow(kDateFormat)
    }
    
    override init(realm: RLMRealm, schema: RLMObjectSchema) {
        super.init(realm: realm, schema: schema)
    }
    
    dynamic func getProgressOfReading() -> Float {
        let book = linkingObjects(Book.self, forProperty: "_bookLogs")[0]
        if (book.numberOfPages != 0) {
            return Float(_numberOfReadPages) / Float(book.numberOfPages)
        }
        return 0
    }


    
}
