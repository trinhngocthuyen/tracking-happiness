//
//  BooksViewModel.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/7/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import Foundation

class BooksViewModel: NSObject {
    
    // MARK: - Private properties
    dynamic private var _bookItemViewModels: [BookItemViewModel] = []
    
    // MARK: - Public Properties
    dynamic var bookItemViewModels: [BookItemViewModel] {
        get {
            return _bookItemViewModels
        }
        set {
            _bookItemViewModels = newValue
        }
    }
    
    
    // MARK: - Initializers
    override init() {
        super.init()
    }
    
    convenience init(bookItemViewModels: [BookItemViewModel]) {
        self.init()
        self._bookItemViewModels = bookItemViewModels
    }
    
   
}
