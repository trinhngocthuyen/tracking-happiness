//
//  BookNoteItemViewModel.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/16/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import Foundation

class BookNoteItemViewModel: NSObject {
    
    // MARK: - Private properties
    dynamic private var _bookNoteContent: String = ""
    dynamic private var _bookNoteTimestamp: String = ""
    
    
    // MARK: - Public methods
    dynamic var bookNote = BookNote()
    dynamic var bookNoteContent: String {
        get { return _bookNoteContent }
    }
    
    dynamic var bookNoteTimestamp: String {
        get { return _bookNoteTimestamp }
    }
    
    override init() {
        super.init()
        
        RAC(self, "_bookNoteContent") <~ RACObserve(self, "bookNote.content")
        RAC(self, "_bookNoteTimestamp") <~ RACObserve(self, "bookNote.timestamp")
        
    }
    
    convenience init(bookNote: BookNote) {
        self.init()
        self.bookNote = bookNote
    }
}
