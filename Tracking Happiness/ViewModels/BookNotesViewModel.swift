//
//  NotesViewModel.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/16/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import Foundation

class BookNotesViewModel: NSObject {

    // MARK: - Private properties
    dynamic private var _book: Book = Book()
    dynamic private var _bookNoteItemViewModels: [BookNoteItemViewModel] = []
    
    // MARK: - Public Properties
    dynamic var book: Book {
        get { return _book }
        set { _book = newValue }
    }
    dynamic var bookNoteItemViewModels: [BookNoteItemViewModel] {
        get { return _bookNoteItemViewModels }
        set { _bookNoteItemViewModels = newValue }
    }
    
    override init() {
        super.init()
    }
    
    convenience init(book: Book) {
        self.init()
        _book = book
    }
    
    convenience init(book: Book, bookNoteItemViewModels: [BookNoteItemViewModel]) {
        self.init()
        _book = book
        _bookNoteItemViewModels = bookNoteItemViewModels
    }
}