//
//  ReadingProgressViewModel.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/18/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import Foundation
import ReactiveCocoa
import RealmSwift

class ReadingProgressViewModel: NSObject {
    dynamic private var _numberOfReadPages: Int = 0
    dynamic private var _numberOfTotalPages: Int = 0
    dynamic private var _progressOfReading: Float = 0
    
    dynamic var book: Book = Book()
    
    dynamic var numberOfReadPages: Int {
        get { return _numberOfReadPages }
    }
    
    dynamic var numberOfTotalPages: Int {
        get { return _numberOfTotalPages }
    }
    
    dynamic var progressOfReading: Float {
        get { return _progressOfReading }
    }
    
    override init() {
        super.init()
        
        RACObserve(self, "book").subscribeNextAs() { (nextBook: Book) in
            self._numberOfTotalPages = nextBook.numberOfPages
            self._numberOfReadPages = nextBook.getCurrentNumberOfReadPages()
            self._progressOfReading = nextBook.getCurrentProgressOfReading()
            
            NSLog("nextBook.getCurrentNumberOfReadPages() = \(nextBook.getCurrentNumberOfReadPages())")
            NSLog("_numberOfReadPages = \(nextBook.getCurrentNumberOfReadPages())")
            NSLog("_progressOfReading = \(nextBook.getCurrentProgressOfReading())")
        }
    }
    
    convenience init(book: Book) {
        self.init()
        self.book = book
    }
    
    
}