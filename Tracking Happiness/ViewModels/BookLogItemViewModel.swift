//
//  BookItemViewModel.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/12/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import Foundation
import ReactiveCocoa

class BookItemViewModel: NSObject {
    
    // MARK: - Private properties
    dynamic private var _bookTitle: String = ""
    dynamic private var _progressOfReading: Float = 0
    
    // MARK: - Public properties
    dynamic var book: Book = Book()
    
    dynamic var bookTitle: String {
        get {
            return _bookTitle
        }
    }
    
    dynamic var progressOfReading: Float {
        get {
            return _progressOfReading
        }
    }

    
    override init() {
        super.init()

        RAC(self, "_bookTitle", nilValue: "") <~ RACObserve(self, "book.title")
        RAC(self, "_progressOfReading", nilValue: 0) <~ RACObserve(self, "book.getCurrentProgressOfReading")

    }
    
    convenience init(book: Book) {
        self.init()
        self.book = book
    }
    
}
