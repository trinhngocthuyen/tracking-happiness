//
//  Constants.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/9/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import Foundation

// MARK: - Identifiers
let kIdentifierBookItemTableViewCell = "BookItemTableViewCell"
let kIdentifierBookItemSectionHeaderView = "BookItemSectionHeaderView"
let kIdentifierBookNoteItemTableViewCell = "BookNoteItemTableViewCell"


// MARK: - Segue identifiers
let kSegueBookNotes =  "BookNotes"
let kSegueBookNoteDetails = "BookNoteDetails"
let kSegueAddNewBookNote = "AddNewBookNote"
let kSegueEditBookNote = "EditBookNote"
let kSegueEditBook = "EditBook"



// MARK: - View controller titles
let kViewControllerTitleBooks = "Books"
let kViewControllerReflection = "Reflection"
let kViewControllerTimeline = "Timeline"
let kViewControllerActivities = "Activities"

// MARK: - Date Time
let kDateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
let kDateFormatOfToday = "yyyy-MM-dd"

// MARK: - Realm
let kStatusReading = "Reading"
let kStatusPending = "Pending"

// MARK: - UI
let kProgressTotal = UInt(100)

