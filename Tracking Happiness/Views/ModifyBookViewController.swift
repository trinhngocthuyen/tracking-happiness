//
//  ModifyBookViewController.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/21/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import TextFieldEffects
import ReactiveCocoa
import RealmSwift


class ModifyBookViewController: UIViewController {
    
    // MARK: - Properties
    
    @IBOutlet weak var titleTextField: HoshiTextField!
    @IBOutlet weak var authorTextField: HoshiTextField!
    @IBOutlet weak var numberOfPagesTextField: HoshiTextField!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    // MARK: - Life cycle methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpValidation()
        setUpView()
    }
    
    func setUpValidation() {
        
        // MARK: Ensure all fields are not empty
        /*
        * NOTE: textView.rac_textSignal() does not trigger text changes by setting programmatically
        * RACObserve(textView, "text") does not trigger text changes by typing from the keyboard
        */
        
        // Signal `field title is valid`
        let titleSignal1 = titleTextField.rac_textSignal()
        let titleSignal2 = RACObserve(self.titleTextField, "text")
        
        let validTitleSignal = titleSignal1.merge(titleSignal2)
            .mapAs() { (text: String) -> AnyObject in
            return !text.isEmpty
        }
        
        // Signal `field author is valid`
        let authorSignal1 = authorTextField.rac_textSignal()
        let authorSignal2 = RACObserve(self.authorTextField, "text")
        
        let validAuthorSignal = authorSignal1.merge(authorSignal2)
            .mapAs() { (text: String) -> AnyObject in
            return !text.isEmpty
        }
        
        // Signal `field number of pages is valid`
        let numberOfPagesSignal1 = numberOfPagesTextField.rac_textSignal()
        let numberOfPagesSignal2 = RACObserve(self.numberOfPagesTextField, "text")
        
        let validNumberOfPagesSignal = numberOfPagesSignal1.merge(numberOfPagesSignal2)
            .mapAs() { (text: String) -> AnyObject in
            return !text.isEmpty
        }
        
        // Signal `all fields is valid`
        let validFieldsSignal = RACSignal.combineLatest([validTitleSignal, validAuthorSignal, validNumberOfPagesSignal]).map() {
            let tuple = $0 as! RACTuple
            let bools = tuple.allObjects() as! [Bool]
            return bools.reduce(true) { (b1: Bool, b2: Bool) in
                return b1 && b2
            }
        }
        
        // Disable the `done button` if there is one invalid field
        validFieldsSignal.subscribeNextAs() { (isValid: Bool) -> Void in
            self.doneButton.enabled = isValid
        }
    }
    
    func setUpView() {
        
        // Set titleTextField as the first responder
        titleTextField.becomeFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    // MARK: - Actions
    @IBAction func tapDone(sender: UIBarButtonItem) {
        fatalError("Need to implement me")
    }
    
    @IBAction func tapCancel(sender: UIBarButtonItem) {
        fatalError("Need to implement me")
    }
    
    // MARK: - Private methods
    func createBookFromCurrentContext() -> Book {
        let book = Book()
        book.title = titleTextField.text ?? ""
        book.author = authorTextField.text ?? ""
        book.numberOfPages = numberOfPagesTextField.text.toInt() ?? 0
        return book
    }
    
}

