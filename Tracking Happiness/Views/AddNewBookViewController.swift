//
//  AddNewBookViewController.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/12/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import TextFieldEffects
import ReactiveCocoa
import RealmSwift

class AddNewBookViewController: ModifyBookViewController {
    
    // MARK: - Properties
    
    // MARK: - Life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Actions
    
    
    override func tapDone(sender: UIBarButtonItem) {
        let newBook = createBookFromCurrentContext()
        let newBookLog = BookLog()
        saveObjectToDatabase(newBook)
        navigationController?.popViewControllerAnimated(true)
    }
    
    override func tapCancel(sender: UIBarButtonItem) {
        showAlertToConfirmDiscard()
    }
    
    // MARK: - Private methods
    func showAlertToConfirmDiscard() {
        IOSAlertShower.showAlert(title: "Discard book", message: "If you go back now, this book will be discarded", cancelButtonTitle: "Keep", okButtonTitle: "Discard", cancelHandler: nil) {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    func saveObjectToDatabase<T: Object>(newObject: T) {
        let realm = Realm()
        
        realm.write() {
            realm.add(newObject, update: true)
        }
        
    }
}

