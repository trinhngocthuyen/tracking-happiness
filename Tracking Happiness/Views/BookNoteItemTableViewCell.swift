//
//  NoteItemTableViewCell.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/15/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit

class BookNoteItemTableViewCell: UITableViewCell {

    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var timestampLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func bindViewModel(viewModel: BookNoteItemViewModel) {
        contentLabel.text = viewModel.bookNoteContent
        timestampLabel.text = getRelativeTimestampString(viewModel.bookNoteTimestamp)

    }
    
    func getRelativeTimestampString(dateString: String) -> String {
        let now = NSDate(timeIntervalSinceNow: 0)
        let date = NSDate.getDateFromTimestamp(dateString, dateFormat: kDateFormat)
        
        let diffDays = date?.daysBeforeDate(now) ?? 0
        if diffDays >= 1 {
            return date?.toString(format: .Custom("dd MMM HH:mm")) ?? dateString
        }
        
        let diffHours = date?.hoursBeforeDate(now) ?? 0
        if diffHours > 1 {
            return "\(diffHours) hours ago"
        } else if diffHours == 1 {
            return "1 hour ago"
        }
        
        let diffMinutes = date?.minutesBeforeDate(now) ?? 0
        if diffMinutes > 1 {
            return "\(diffMinutes) minutes ago"
        } else if diffMinutes == 1 {
            return "1 minute ago"
        }
        
        return "Just now"
    }
}
