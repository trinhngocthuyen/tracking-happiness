//
//  BracketTextView.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/20/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit

//@IBDesignable
class BracketTextView: UITextView {
    
    @IBInspectable var color = UIColor.blackColor() {
        didSet { setNeedsDisplay() }
    }
    
    @IBInspectable var lineWidth = CGFloat(3) {
        didSet { setNeedsDisplay() }
    }
    
    @IBInspectable var bracketWidth = CGFloat(10) {
        didSet { setNeedsDisplay() }
    }
    
    @IBInspectable var bracketInsets = UIEdgeInsetsMake(5, 5, 5, 5) {
        didSet { setNeedsDisplay() }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setUpInsets()
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        setUpInsets()
    }
    
    func setUpInsets() {
        self.textContainerInset = UIEdgeInsetsMake(15, 15, 15, 15)
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        drawLeftBracketPath()
        drawRightBracketPath()
    }
    
    func drawLeftBracketPath() {
        let contentHeight = self.contentSize.height
        
        let topLeftCorner = CGPointMake(bracketInsets.left, bracketInsets.top)
        let bottomLeftCorner = CGPointMake(topLeftCorner.x, contentHeight)
        let topLeft2 = CGPointMake(topLeftCorner.x + bracketWidth, topLeftCorner.y)
        let bottomLeft2 = CGPointMake(bottomLeftCorner.x + bracketWidth, bottomLeftCorner.y)
        
        let leftVerticalPath = UIBezierPath()
        leftVerticalPath.lineWidth = lineWidth
        color.setStroke()
        
        leftVerticalPath.moveToPoint(topLeft2)
        leftVerticalPath.addLineToPoint(topLeftCorner)
        leftVerticalPath.addLineToPoint(bottomLeftCorner)
        leftVerticalPath.addLineToPoint(bottomLeft2)
        
        leftVerticalPath.stroke()
        leftVerticalPath.closePath()
    }
    
    func drawRightBracketPath() {
        let contentHeight = self.contentSize.height
        
        let topRightCorner = CGPointMake(self.frame.width - bracketInsets.right, bracketInsets.top)
        let bottomRightCorner = CGPointMake(topRightCorner.x, contentHeight)
        let topRight2 = CGPointMake(topRightCorner.x - bracketWidth, topRightCorner.y)
        let bottomRight2 = CGPointMake(bottomRightCorner.x - bracketWidth, bottomRightCorner.y)
        
        let rightVerticalPath = UIBezierPath()
        rightVerticalPath.lineWidth = lineWidth
        color.setStroke()
        
        rightVerticalPath.moveToPoint(topRight2)
        rightVerticalPath.addLineToPoint(topRightCorner)
        rightVerticalPath.addLineToPoint(bottomRightCorner)
        rightVerticalPath.addLineToPoint(bottomRight2)
        
        rightVerticalPath.stroke()
        rightVerticalPath.closePath()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setNeedsDisplay()
    }
    
}
