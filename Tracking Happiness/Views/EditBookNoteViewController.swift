//
//  EditBookNoteViewController.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/19/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

/**
*  Responsibilities:
*  - Edit a new book note
*
*  NOTE: See super class `ModifyBookNoteViewController` for more setting
*/

class EditBookNoteViewController: ModifyBookNoteViewController {
    
    dynamic var bookNote: BookNote = BookNote()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentTextView.text = bookNote.content

    }
    
    override func tapDone(sender: AnyObject) {
        // Collect info to create a new note
        let newNote = createBookNoteFromCurrentContext()
        
        // Add newNote the book's notes
        Realm().write {
            //self.book.notes.append(newNote)
            self.bookNote.content = newNote.content
        }
        
        // Pop view controller and go back
        self.navigationController?.popViewControllerAnimated(true)
    }

    override func tapCancel(sender: AnyObject) {
        showAlertToConfirmDiscard()
    }
    
    func showAlertToConfirmDiscard() {
        IOSAlertShower.showAlert(title: "Discard changes", message: "If you go back now, your changes will be discarded", cancelButtonTitle: "Keep", okButtonTitle: "Discard", cancelHandler: nil) {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }

    
}
