//
//  BookItemTableViewCell.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/6/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import ReactiveCocoa

class BookItemTableViewCell: UITableViewCell {

    @IBOutlet weak var bookTitleLabel: UILabel!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func bindViewModel(viewModel: BookItemViewModel) {
        bookTitleLabel.text = viewModel.bookTitle
        progressView.progress = viewModel.progressOfReading
        progressLabel.text = viewModel.progressOfReading.toPercentageString()
    }
    
}
