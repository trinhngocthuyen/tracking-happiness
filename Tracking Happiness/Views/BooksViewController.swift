//
//  BooksViewController.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/7/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import RealmSwift

let NUMBER_OF_SECTIONS = 2
let SECTION_INDEX_READING_BOOKS = 0
let SECTION_TITLE_READING_BOOKS = "Currently reading"
let SECTION_INDEX_PENDING_BOOKS = 1
let SECTION_TITLE_PENDING_BOOKS = "Bookshelf"

//  NOTE:
//  Section 1: Reading books
//  Section 2: Pending books
//  By default, `Pending books` section contains unread books (progress = 0).
//  Users can also move a currently reading book to this section

class BooksViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - Properties
    @IBOutlet weak var tableView: UITableView!
    dynamic var readingBooksViewModel: BooksViewModel = BooksViewModel()
    dynamic var pendingBooksViewModel: BooksViewModel = BooksViewModel()
    
    var realmNotificationToken: NotificationToken?  // notification for realm models changes

    // MARK: - Life cycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Views: Reading books and Pending books share the same table view
        setUpReadingAndPendingBooksSharedView()
        
        // Reading books
        setUpPendingBooksViewViewModel()
        
        // Pending books
        setUpReadingBooksViewViewModel()
        
        // Reaction to Database modification
        /**
        * TODO: You may not want to react to all changes
        * Add `conditions or filters` for `notifications, realm`
        * Or consider using RACObserve. See `setUpReadingBooksViewModel()` and `setUpPendingBooksViewModel()`
        */
        realmNotificationToken = Realm().addNotificationBlock() { notification, realm in
            NSLog("Notification = \(notification)")
            NSLog("Realm = \(realm)")
            
            self.tableView.reloadData()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // Fetch reading books from local
        updateReadingBooksModel()
        
        // Fetch pending books from local
        updatePendingBooksModel()
        
        // Reload table view
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Set up `views`
    func setUpReadingAndPendingBooksSharedView() {
        setUpTableView()
    }
    
    func setUpTableView() {
        // Assign itself as a datasource and a delegate
        tableView.dataSource = self
        tableView.delegate = self
        
        // Register reuse cell
        let bookItemTableViewCellNib = UINib(nibName: "BookItemTableViewCell", bundle: nil)
        let bookLogSectionHeaderCellNib = UINib(nibName: "BookItemSectionHeaderTableViewCell", bundle: nil)
        tableView.registerNib(bookItemTableViewCellNib, forCellReuseIdentifier: kIdentifierBookItemTableViewCell)
        tableView.registerNib(bookLogSectionHeaderCellNib, forCellReuseIdentifier: kIdentifierBookItemSectionHeaderView)
    }
    
    func setUpReadingBooksView() {
        /**
        * TODO: You may want to change cell configuration to distinguish reading books from pending books
        * Example:  + Change background color
        *           + Use different cells
        */
    }
    
    func setUpPendingBooksView() {
        /**
        * TODO: You may want to change cell configuration to distinguish reading books from pending books
        * Example:  + Change background color
        *           + Use different cells
        */
    }
    
    // MARK: - Set up `view models`
    func setUpReadingBooksViewModel() {
        /**
        * TODO: You may want to set up RACObserve for readingBooksViewModel
        * Example:  + Reload table view
        * Since ReactiveCocoa not yet support observing the `whole properties` and `nested observation`,
        the observation may not be captured sometimes.
        * Use `Realm notification` instead
        */
        
        /*
        let readingBooksViewModelSignal = RACObserve(self, "readingBooksViewModel.bookItemViewModels").ignore(nil)
        readingBooksViewModelSignal.subscribeNextAs() { (bookItemViewModels: [BookItemViewModel]) in
            self.tableView.reloadData()
        }*/
    }
    
    func setUpPendingBooksViewModel() {
        /**
        * TODO: You may want to set up RACObserve for pendingBooksViewModel
        * Example:  + Reload table view
        * Since ReactiveCocoa not yet support observing the `whole properties` and `nested observation`,
        the observation may not be captured sometimes.
        * Use `Realm notification` instead
        */
        
        /*
        let pendingBooksViewModelSignal = RACObserve(self, "pendingBooksViewModel.bookItemViewModels").ignore(nil)
        pendingBooksViewModelSignal.subscribeNextAs() { (bookItemViewModels: [BookItemViewModel]) in
            self.tableView.reloadData()
        }
        */
    }
    
    // MARK: - Set up `views - view models`
    func setUpReadingBooksViewViewModel() {
        setUpReadingBooksView()
        setUpReadingBooksViewModel()
    }
    
    func setUpPendingBooksViewViewModel() {
        setUpPendingBooksView()
        setUpPendingBooksViewModel()
    }
    
    // MARK: - Update `models`
    func updateReadingBooksModel() {
        let books = Array(Realm().objects(Book))
        readingBooksViewModel.bookItemViewModels = createBookItemViewModels(books, status: kStatusReading)
        NSLog("Fetch model: reading books model = \(readingBooksViewModel)")
    }
    
    func updatePendingBooksModel() {
        let books = Array(Realm().objects(Book))
        pendingBooksViewModel.bookItemViewModels = createBookItemViewModels(books, status: kStatusPending)
        NSLog("Fetch model: pending books model = \(readingBooksViewModel)")
    }
    
    func createBookItemViewModels(books: Array<Book>, status: String) -> Array<BookItemViewModel> {
        let filteredBooks = books.filter() { book in
            return book.status == status
        }
        
        let filteredBookItemViewModel = filteredBooks.map() { book -> BookItemViewModel in
            let bookItemViewModel = BookItemViewModel()
            bookItemViewModel.book = book
            return bookItemViewModel
        }
        return filteredBookItemViewModel
    }
    
    // MARK: - UITableViewDataSource methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return NUMBER_OF_SECTIONS
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return getViewModelOfSection(section)?.bookItemViewModels.count ?? 0
    }
    
    func getViewModelOfSection(section: Int) -> BooksViewModel? {
        switch (section) {
        case SECTION_INDEX_READING_BOOKS:
            return readingBooksViewModel
        case SECTION_INDEX_PENDING_BOOKS:
            return pendingBooksViewModel
        default:
            return nil
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(kIdentifierBookItemTableViewCell) as? BookItemTableViewCell
        
        let booksViewModel = getViewModelOfSection(indexPath.section)!
        let cellViewModel: BookItemViewModel = booksViewModel.bookItemViewModels[indexPath.row]
        cell?.bindViewModel(cellViewModel)
        
        return cell!
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableCellWithIdentifier(kIdentifierBookItemSectionHeaderView) as? BookItemSectionHeaderTableViewCell
        let title: String?
        switch (section) {
        case SECTION_INDEX_READING_BOOKS:
            title = SECTION_TITLE_READING_BOOKS
        case SECTION_INDEX_PENDING_BOOKS:
            title = SECTION_TITLE_PENDING_BOOKS
        default:
            title = ""
        }
        
        headerView?.titleLabel.text = title!
        
        return headerView!
    }
    
    // MARK: - UITableViewDelegate methods
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let book = getBookForSegue(indexPath)
        performSegueWithIdentifier(kSegueBookNotes, sender: book)
    }
    
    func getBookForSegue(indexPath: NSIndexPath) -> Book {
        let bookItemViewModel = getViewModelOfSection(indexPath.section)?.bookItemViewModels[indexPath.row]
        return bookItemViewModel?.book ?? Book()
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Pass the current `book` to `BookNotesViewController`
        if segue.identifier == kSegueBookNotes {
            if let  book = sender as? Book, bookNotesViewController = segue.destinationViewController as? BookNotesViewController {
                bookNotesViewController.book = book
            }
        }
    }
}


