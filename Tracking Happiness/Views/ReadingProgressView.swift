//
//  ReadingProgressView.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/18/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import MDRadialProgress

class ReadingProgressView: UIView {

    @IBOutlet weak var circularProgressView: MDRadialProgressView!
    @IBOutlet weak var numberOfReadPagesLabel: UILabel!
    @IBOutlet weak var numberOfTotalPagesLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        circularProgressView.theme.completedColor = UIColor.whiteColor()
        circularProgressView.theme.incompletedColor = UIColor.clearColor()
        circularProgressView.theme.sliceDividerHidden = true
        circularProgressView.theme.thickness = 15
        circularProgressView.theme.centerColor = UIColor.clearColor()
        circularProgressView.theme.labelShadowColor = UIColor.clearColor()
        circularProgressView.label.textColor = UIColor.yellowColor()
        circularProgressView.progressTotal = kProgressTotal
        circularProgressView.progressCounter = 0
    }
    
    func bindViewModel(viewModel: ReadingProgressViewModel) {
        numberOfReadPagesLabel.text = "\(viewModel.numberOfReadPages)"
        numberOfTotalPagesLabel.text = "\(viewModel.numberOfTotalPages)"
        circularProgressView.progressTotal = kProgressTotal
        circularProgressView.progressCounter = UInt(viewModel.progressOfReading * 100)
    }
}
