//
//  BookItemSectionHeaderTableViewCell.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/14/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit

class BookItemSectionHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
