//
//  BookNoteDetailViewController.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/19/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import ReactiveCocoa
import RealmSwift
import Realm

/**
*  Responsibilities:
*  - Show details of a book note (selected from the previous screen)
*  - Navigate to edit screen
*  - Delete the current note
*/

class BookNoteDetailsViewController: UIViewController {
    
    @IBOutlet weak var contentTextView: BracketTextView!
    dynamic var bookNote: BookNote = BookNote()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpView()
    }
    
    func setUpView() {
        RAC(self, "contentTextView.text", nilValue: "") <~ RACObserve(self, "bookNote.content").ignore(nil)
    }

    
    @IBAction func tapDelete(sender: AnyObject) {
        showAlertToConfirmDelete()
    }
    
    func showAlertToConfirmDelete() {
        IOSAlertShower.showAlert(title: "Delete note", message: "Are you sure you want to delete this note", cancelButtonTitle: "Cancel", okButtonTitle: "Delete", cancelHandler: nil) {
            
            let realm = Realm()
            realm.write() {
                realm.delete(self.bookNote)
                
                /// NOTE: init a book after deleting to prevent the app from crashing
                self.bookNote = BookNote()
            }
            
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    // MARK: - Navigation
    @IBAction func tapEdit(sender: AnyObject) {
        performSegueWithIdentifier(kSegueEditBookNote, sender: bookNote)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let identifier = segue.identifier {
            switch (identifier) {
                // Next view controller is `EditBookNoteViewController`
            case kSegueEditBookNote:
                if let bookNote = sender as? BookNote, editBookNoteViewController = segue.destinationViewController as? EditBookNoteViewController {
                    editBookNoteViewController.bookNote = bookNote
                }
            default:
                return
            }
        }
    }
    
    
}
