//
//  BookLogsDetails.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/15/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import Foundation
import ReactiveCocoa
import RealmSwift
import Realm
import MDRadialProgress


class BookNotesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var bookTitleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var readingProgressView: ReadingProgressView!
    
    // MARK: - Models & View models
    /**
    *  Model: book
    *  ViewModel: `bookNotesViewModel`: show book notes in the table
    *  ViewModel:   `readingProgressViewModel`: show current progress of reading
    *
    *  NOTE:
    *    + book: is passed via segue
    *    + realmNotificationToken [required]: notification for realm models changes, use to unsubscribe notification
    */
    
    dynamic var book: Book = Book()                 // book is passed via segue
    dynamic var bookNotesViewModel = BookNotesViewModel()
    dynamic var readingProgressViewModel = ReadingProgressViewModel()
    
    var realmNotificationToken: NotificationToken?  // notification for realm models changes
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Book details
        setUpBookDetailsView()
        
        // Book notes
        setUpBookNotesViewViewModel()
        
        // Reading progress
        setUpReadingProgressViewViewModel()
        
        // Reaction to Database modification
        realmNotificationToken = Realm().addNotificationBlock() { notification, realm in
            NSLog("Notification = \(notification)")
            NSLog("Realm = \(realm)")
            
            self.updateBookDetailsViewViewModel()
            self.updateBookNotesViewViewModel()
            self.updateReadingProgressViewViewModel()
            
        }
    }
    
    // MARK: - Set up `views`
    func setUpBookDetailsView() {
        bookTitleLabel.text = book.title
    }
    
    func setUpTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 60
        
        let noteItemTableViewCellNib = UINib(nibName: "BookNoteItemTableViewCell", bundle: nil)
        tableView.registerNib(noteItemTableViewCellNib, forCellReuseIdentifier: kIdentifierBookNoteItemTableViewCell)
        
    }
    
    func setUpReadingProgressView() {
        // TODO: Config colors, themes.
        // Also change in the file 'ReadingProgressView.swift'
    }
    
    // MARK: - Set up `view models`
    func setUpBookNotesViewModel() {
        RACObserve(self, "book").subscribeNextAs() { (nextBook: Book) in
            self.bookNotesViewModel.book = nextBook
        }
    }
    
    func createBookNoteItemViewModels(bookNotes: Array<BookNote>) -> [BookNoteItemViewModel] {
        return bookNotes.map() { bookNote -> BookNoteItemViewModel in
            let bookNoteItemViewModel = BookNoteItemViewModel()
            bookNoteItemViewModel.bookNote = bookNote
            return bookNoteItemViewModel
        }
    }
    
    func setUpReadingProgressViewModel() {
        RACObserve(self, "book").subscribeNextAs() { (nextBook: Book) in
            self.updateReadingProgressViewModel()
            self.readingProgressView.bindViewModel(self.readingProgressViewModel)
        }
    }
    
    // MARK: - Update `view models`
    func updateBookNotesViewModel() {
        if (book.realm != nil) {
            // Fetch bookNotes from database
            // Order of appearance: newest on the top
            let bookNotes = Array(book.notes.sorted("_timestamp", ascending: false))
                bookNotesViewModel.bookNoteItemViewModels = createBookNoteItemViewModels(bookNotes)
        }
    }
    
    func updateBookDetailsViewModel() {
        /// NOTE: Do nothing because the view controller is holding a reference to `book`
    }
    
    func updateReadingProgressViewModel() {
        self.readingProgressViewModel.book = self.book
    }
    
    // MARK: - Set up `views - view models`
    func setUpBookNotesViewViewModel() {
        setUpBookNotesViewModel()
        setUpTableView()
    }
    
    func setUpReadingProgressViewViewModel() {
        setUpReadingProgressViewModel()
        setUpReadingProgressView()
    }
    
    // MARK: - Update `views - view models`
    func updateBookDetailsViewViewModel() {
        self.updateBookDetailsViewModel()
        self.setUpBookDetailsView()
    }
    
    func updateBookNotesViewViewModel() {
        self.updateBookNotesViewModel()
        self.tableView.reloadData()
    }
    
    func updateReadingProgressViewViewModel() {
        self.updateReadingProgressViewModel()
        self.readingProgressView.bindViewModel(self.readingProgressViewModel)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        updateBookNotesViewModel()
        tableView.reloadData()
    }
    
    // MARK: - Update `models`
    @IBAction func tapUpdateProgress(sender: AnyObject) {
        showAlertToUpdateProgress()
    }
    
    func showAlertToUpdateProgress() {
        // Show an alert to input a new record
        IOSAlertShower.showAlertWithTextField(title: "Update progress", message: "Enter new number of read pages", placeHolder: "Number of pages", cancelButtonTitle: "Cancel", okButtonTitle: "Update", cancelHandler: nil) { textField in
            if let newNumberOfReadPages = textField.text.toInt() {
                self.updateProgressOfReading(newNumberOfReadPages)
            }
        }
    }
    
    /**
    * Update progress of reading
    * by update the latest `book log` (in the `same day`)
    * NOTE: There is `at most` 1 book log per day
    */
    func updateProgressOfReading(newNumberOfReadPages: Int) {
        
        /**
        * NOTE: Make sure `kDateFormatOfToday` resembles `kDateFormat`
        * Example:    kDateFormat:        "yyyy-MM-dd'T'HH:mm:ss.SSS'Z"
        *             kDateFormatOfToday: "yyyy-MM-dd"
        *
        * TODO: 1) Consider change the predicate to reduce the dependency
        *       between `kDateFormat` and `kDateFormatOfToday`
        *       2) You may want to change the type of `timestamp` from `String` to `NSDate`
        *       In that case, change the predicate
        */
        
        let dateOfTodayString = NSDate.getTimeStampOfNow(kDateFormatOfToday)
        let isTodayPredicate = NSPredicate(format: "_timestamp CONTAINS %@", dateOfTodayString)
        if let todayBookLog = book.bookLogs.filter(isTodayPredicate).first {
            Realm().write() {
                todayBookLog.numberOfReadPages = newNumberOfReadPages
                NSLog("Update book log: new record = \(newNumberOfReadPages) pages")
                NSLog("Update book log: book log = \(todayBookLog)")
            }
        } else {
            updateProgressOfReadingWithNewBookLog(newNumberOfReadPages)
        }
    }
    
    /**
    * Update progress of reading
    * by creating a new `book log` and append to the `book log list`
    */
    func updateProgressOfReadingWithNewBookLog(newNumberOfReadPages: Int) {
        
        // Create a new book log
        let newBookLog = BookLog()
        newBookLog.numberOfReadPages = newNumberOfReadPages
        
        Realm().write() {
            self.book.bookLogs.append(newBookLog)
        }
        
        Realm().refresh()
    }
    
    // MARK: - UITableViewDataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return bookNotesViewModel.bookNoteItemViewModels.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(kIdentifierBookNoteItemTableViewCell) as? BookNoteItemTableViewCell
        let bookNoteItemViewModel = bookNotesViewModel.bookNoteItemViewModels[indexPath.row]
        cell?.bindViewModel(bookNoteItemViewModel)
        return cell!
    }
    
    // MARK: - Navigation
    // MARK: UITableViewDelegate
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let selectedBookNoteItemViewModel = bookNotesViewModel.bookNoteItemViewModels[indexPath.row]
        let selectedBookNote = selectedBookNoteItemViewModel.bookNote
        
        // Navigate to `AddNewBookNoteViewController`
        // Data for next view controleller: book
        performSegueWithIdentifier(kSegueBookNoteDetails, sender: selectedBookNote)
    }
    
    @IBAction func tapAddBookNote(sender: AnyObject) {
        // Navigate to `AddNewBookNoteViewController`
        // Data for next view controleller: book
        performSegueWithIdentifier(kSegueAddNewBookNote, sender: book)
    }
    
    @IBAction func tapEditBook(sender: AnyObject) {
        // Navigate to `EditBookViewController`
        // Data for next view controleller: book
        performSegueWithIdentifier(kSegueEditBook, sender: book)
    }
    
    @IBAction func tapDeleteBook(sender: AnyObject) {
        showAlertToConfirmDelete()
    }
    
    func showAlertToConfirmDelete() {
        IOSAlertShower.showAlert(title: "Delete book", message: "If you delete this book. All notes and progress tracking will be deleted", cancelButtonTitle: "Cancel", okButtonTitle: "Delete", cancelHandler: nil) {
            
            let realm = Realm()
            realm.write() {
                realm.delete(self.book)
                /**
                * NOTE: Since `updateBookNotesViewModel` and `updateReadingProgressViewViewModel` hold a reference to book,
                * init a book to prevent the app from crashing
                */
                self.book = Book()
            }
            
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if let identifier = segue.identifier {
            switch (identifier) {
                
                // Next view controller is `AddNewBookNoteViewController`
            case kSegueAddNewBookNote:
                if let  book = sender as? Book, addNewBookNoteViewController = segue.destinationViewController as? AddNewBookNoteViewController {
                    addNewBookNoteViewController.book = book
                }

                // Next view controller is `BookNoteDetailsViewController`
            case kSegueBookNoteDetails:
                if let  bookNote = sender as? BookNote, bookNoteDetailsViewController = segue.destinationViewController as? BookNoteDetailsViewController {
                    bookNoteDetailsViewController.bookNote = bookNote
                }
                
                // Next view controller is `EditBookViewController`
            case kSegueEditBook:
                if let  book = sender as? Book, editBookViewController = segue.destinationViewController as? EditBookViewController {
                    editBookViewController.book = book
                }
                
            default:
                return
            }
        }
        
    }
    
}
