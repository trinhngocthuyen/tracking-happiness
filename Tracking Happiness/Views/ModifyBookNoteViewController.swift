//
//  ModifyBookViewController.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/19/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class ModifyBookNoteViewController: UIViewController {
    // MARK: - Properties
    // NOTE: In EditBookNoteViewController: change title of doneButton to `Save` instead of `Done`
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var contentTextView: UITextView!
    
    dynamic var book: Book = Book()
    
    // MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpValidation()
        setUpView()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpView() {
        // Set contentTextView as the first responder
        contentTextView.becomeFirstResponder()
        
        // Align text to the top of text view
        // self.automaticallyAdjustsScrollViewInsets = false
    }
    
    func setUpValidation() {
        // MARK: Ensure all fields are not empty
        /*
        * NOTE: textView.rac_textSignal() does not trigger text changes by setting programmatically
        * RACObserve(textView, "text") does not trigger text changes by typing from the keyboard
        */
        
        // Signal `field content is valid`
        let textSignal1 = contentTextView.rac_textSignal()
        let textSignal2 = RACObserve(self.contentTextView, "text")
        
        let validContentSignal = textSignal1.merge(textSignal2)
            .mapAs() { (text: String) -> AnyObject in
            return !text.isEmpty
        }
        
        // Disable the `done button` if there is one invalid field
        validContentSignal.subscribeNextAs() { (isValid: Bool) in
            self.doneButton.enabled = isValid
        }
    }
    
    // MARK: - Actions
    @IBAction func tapDone(sender: AnyObject) {
        fatalError("Need to implement tapDone")
    }
    
    func createBookNoteFromCurrentContext() -> BookNote {
        let bookNote = BookNote()
        bookNote.content = contentTextView.text ?? ""
        return bookNote
    }
    
    @IBAction func tapCancel(sender: AnyObject) {
        fatalError("Need to implement tapCancel")
    }
    
    
}
