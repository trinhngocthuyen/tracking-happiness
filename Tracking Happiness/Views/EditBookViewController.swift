//
//  EditBookViewController.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/21/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class EditBookViewController: ModifyBookViewController {
    
    // MARK: - Properties
    dynamic var book: Book = Book()
    
    // MARK: - Life cycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleTextField.text = book.title
        authorTextField.text = book.author
        numberOfPagesTextField.text = "\(book.numberOfPages)"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Actions
    override func tapDone(sender: UIBarButtonItem) {

        // Collect info to create a new note
        let newBook = createBookFromCurrentContext()
        
        // Add newNote the book's notes
        Realm().write {
            //self.book.notes.append(newNote)
            self.book.title = newBook.title
            self.book.author = newBook.author
            self.book.numberOfPages = newBook.numberOfPages
        }
        
        // Pop view controller and go back
        navigationController?.popViewControllerAnimated(true)
    }
    
    override func tapCancel(sender: UIBarButtonItem) {
        showAlertToConfirmDiscard()
    }
    
    // MARK: - Private methods
    func showAlertToConfirmDiscard() {
        IOSAlertShower.showAlert(title: "Discard changes", message: "If you go back now, your changes will be discarded", cancelButtonTitle: "Keep", okButtonTitle: "Discard", cancelHandler: nil) {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
}

