//
//  AddNewBookNoteViewController.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/16/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

/**
*  Responsibilities:
*  - Add a new book note
*
*  NOTE: See super class `ModifyBookNoteViewController` for more setting
*/

class AddNewBookNoteViewController: ModifyBookNoteViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentTextView.text = ""
    }

    override func tapDone(sender: AnyObject) {
        // Collect info to create a new note
        let newNote = createBookNoteFromCurrentContext()
        
        // Add newNote the book's notes
        Realm().write {
            self.book.notes.append(newNote)
        }
        
        // Pop view controller and go back
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func tapCancel(sender: AnyObject) {
        showAlertToConfirmDiscard()
    }
    
    func showAlertToConfirmDiscard() {
        IOSAlertShower.showAlert(title: "Discard note", message: "If you go back now, this note will be discarded", cancelButtonTitle: "Keep", okButtonTitle: "Discard", cancelHandler: nil) {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
}
