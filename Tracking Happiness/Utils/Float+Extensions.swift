//
//  StringUtils.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/9/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import Foundation

// MARK: - Float
extension Float {
    func toPercentageString() -> String {
        return String(format: "%.02f", self*100) + "%"
    }
}
