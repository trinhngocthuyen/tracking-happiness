//
//  IOSAlertShower.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/18/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import Foundation

/**
*  Alert shower using UIAlertController
*/
class IOSAlertShower: UIViewController, AlertShower {
    
    
    private static func presentAlertController(#alertController: UIAlertController, animated: Bool) {
        let callerViewController = UIApplication.topViewController()
        callerViewController?.presentViewController(alertController, animated: animated, completion: nil)
    }
    
    /**
    *  Show alert with an OK button
    */
    static func showAlert(#title: String, message: String, okButtonTitle: String, completion: (Void -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        // Add actions to the alert
        let okAlertAction = UIAlertAction(title: okButtonTitle, style: .Default) { action in
            completion?()
        }
        
        alertController.addAction(okAlertAction)
        
        // Present the alert
        self.presentAlertController(alertController: alertController, animated: true)
    }
    
    /**
    *  Show alert with an 2 buttons: Cancel & OK
    */
    static func showAlert(#title: String, message: String, cancelButtonTitle: String, okButtonTitle: String, cancelHandler: (Void -> Void)?, okHandler: (Void -> Void)?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        
        // Add actions to the alert
        let okAlertAction = UIAlertAction(title: okButtonTitle, style: .Default) { action in
            okHandler?()
        }
        let cancelAlertAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) { action in
            cancelHandler?()
        }
        
        alertController.addAction(cancelAlertAction)
        alertController.addAction(okAlertAction)
        
        // Present the alert
        self.presentAlertController(alertController: alertController, animated: true)
    }
    
    /**
    *  Show alert with a `textField` and 2 buttons: Cancel & OK
    */
    static func showAlertWithTextField(#title: String, message: String, placeHolder: String, cancelButtonTitle: String, okButtonTitle: String, cancelHandler: (Void -> Void)?, okHandler: ((textField: UITextField) -> Void)?) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        alertController.addTextFieldWithConfigurationHandler() { textField in
            textField.placeholder = placeHolder
        }
        
        // Add actions to the alert
        let okAlertAction = UIAlertAction(title: okButtonTitle, style: .Default) { action in
            if let textField = alertController.textFields?.first as? UITextField {
                okHandler?(textField: textField)
            }
        }
        let cancelAlertAction = UIAlertAction(title: cancelButtonTitle, style: .Cancel) { action in
            cancelHandler?()
        }
        
        alertController.addAction(cancelAlertAction)
        alertController.addAction(okAlertAction)
        
        // Present the alert
        self.presentAlertController(alertController: alertController, animated: true)
        
    }
}
