//
//  AlertShower.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/18/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import UIKit
import Foundation

/**
*  AlertShower: protocol for showing alert views
*/
protocol AlertShower {
    
    /**
    *  Show alert with an OK button
    */
    static func showAlert(#title: String, message: String, okButtonTitle: String, completion: (Void -> Void)?)
    
    /**
    *  Show alert with an 2 buttons: Cancel & OK
    */
    static func showAlert(#title: String, message: String, cancelButtonTitle: String, okButtonTitle: String, cancelHandler: (Void -> Void)?, okHandler: (Void -> Void)?)
    
    /**
    *  Show alert with a `textField` and 2 buttons: Cancel & OK
    */
    static func showAlertWithTextField(#title: String, message: String, placeHolder: String, cancelButtonTitle: String, okButtonTitle: String, cancelHandler: (Void -> Void)?, okHandler: ((textField: UITextField) -> Void)?)
}

