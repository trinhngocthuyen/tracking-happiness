//
//  RACSignal+Extensions.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/13/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import Foundation
import ReactiveCocoa

// a collection of extension methods that allows for strongly typed closures
extension RACSignal {
    
    func subscribeNextAs<T>(nextClosure:(T) -> ()) -> () {
        self.subscribeNext {
            (next: AnyObject!) -> () in
            let nextAsT = next! as! T
            nextClosure(nextAsT)
        }
    }
    
    func mapAs<T, U: AnyObject>(mapClosure:(T) -> U) -> RACSignal {
        return self.map {
            (next: AnyObject!) -> AnyObject! in
            let nextAsT = next as! T
            return mapClosure(nextAsT)
        }
    }
    
    func filterAs<T>(filterClosure:(T) -> Bool) -> RACSignal {
        return self.filter {
            (next: AnyObject!) -> Bool in
            let nextAsT = next as! T
            return filterClosure(nextAsT)
        }
    }
    
    func doNextAs<T>(nextClosure:(T) -> ()) -> RACSignal {
        return self.doNext {
            (next: AnyObject!) -> () in
            let nextAsT = next as! T
            nextClosure(nextAsT)
        }
    }
}

class RACSignalEx {
    class func combineLatestAs<T, U, R: AnyObject>(signals:[RACSignal], reduce:(T,U) -> R) -> RACSignal {
        return RACSignal.combineLatest(signals).mapAs {
            (tuple: RACTuple) -> R in
            return reduce(tuple.first as! T, tuple.second as! U)
        }
    }
}
