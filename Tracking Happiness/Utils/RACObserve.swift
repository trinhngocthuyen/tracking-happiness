//
//  RAC+RACObserve.swift
//  Tracking Happiness
//
//  Created by Thuyen on 6/12/15.
//  Copyright (c) 2015 Thuyen. All rights reserved.
//

import Foundation
import ReactiveCocoa

// replaces the RACObserve macro
func RACObserve(target: NSObject!, keyPath: String) -> RACSignal  {
    return target.rac_valuesForKeyPath(keyPath, observer: target)
}

