
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// ASProgressPopUpView
#define COCOAPODS_POD_AVAILABLE_ASProgressPopUpView
#define COCOAPODS_VERSION_MAJOR_ASProgressPopUpView 0
#define COCOAPODS_VERSION_MINOR_ASProgressPopUpView 8
#define COCOAPODS_VERSION_PATCH_ASProgressPopUpView 0

// Box
#define COCOAPODS_POD_AVAILABLE_Box
#define COCOAPODS_VERSION_MAJOR_Box 1
#define COCOAPODS_VERSION_MINOR_Box 2
#define COCOAPODS_VERSION_PATCH_Box 2

// MDRadialProgress
#define COCOAPODS_POD_AVAILABLE_MDRadialProgress
#define COCOAPODS_VERSION_MAJOR_MDRadialProgress 1
#define COCOAPODS_VERSION_MINOR_MDRadialProgress 3
#define COCOAPODS_VERSION_PATCH_MDRadialProgress 1

// ReactiveCocoa
#define COCOAPODS_POD_AVAILABLE_ReactiveCocoa
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 3.0-beta.6.

// ReactiveCocoa/Core
#define COCOAPODS_POD_AVAILABLE_ReactiveCocoa_Core
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 3.0-beta.6.

// ReactiveCocoa/UI
#define COCOAPODS_POD_AVAILABLE_ReactiveCocoa_UI
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 3.0-beta.6.

// ReactiveCocoa/no-arc
#define COCOAPODS_POD_AVAILABLE_ReactiveCocoa_no_arc
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 3.0-beta.6.

// Realm
#define COCOAPODS_POD_AVAILABLE_Realm
#define COCOAPODS_VERSION_MAJOR_Realm 0
#define COCOAPODS_VERSION_MINOR_Realm 93
#define COCOAPODS_VERSION_PATCH_Realm 2

// Realm/Headers
#define COCOAPODS_POD_AVAILABLE_Realm_Headers
#define COCOAPODS_VERSION_MAJOR_Realm_Headers 0
#define COCOAPODS_VERSION_MINOR_Realm_Headers 93
#define COCOAPODS_VERSION_PATCH_Realm_Headers 2

// RealmSwift
#define COCOAPODS_POD_AVAILABLE_RealmSwift
#define COCOAPODS_VERSION_MAJOR_RealmSwift 0
#define COCOAPODS_VERSION_MINOR_RealmSwift 93
#define COCOAPODS_VERSION_PATCH_RealmSwift 2

// Result
#define COCOAPODS_POD_AVAILABLE_Result
#define COCOAPODS_VERSION_MAJOR_Result 0
#define COCOAPODS_VERSION_MINOR_Result 4
#define COCOAPODS_VERSION_PATCH_Result 4

// TextFieldEffects
#define COCOAPODS_POD_AVAILABLE_TextFieldEffects
#define COCOAPODS_VERSION_MAJOR_TextFieldEffects 0
#define COCOAPODS_VERSION_MINOR_TextFieldEffects 4
#define COCOAPODS_VERSION_PATCH_TextFieldEffects 0

